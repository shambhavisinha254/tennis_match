class score_board:
    
    def __init__(self):
        self.points = [0, 0]
        self.games = [0, 0]
        self.sets = [0, 0]
        self.point_display = ["L", "15", "30", "40", "A"]

    def __str__(self):
        points_display = f"Points =   {self.point_display[min(self.points[0], 4)]:4} {self.point_display[min(self.points[1], 4)]:4}"
        if self.points[0] == self.points[1] and self.points[0] >= 3:
            points_display = "Points = Deuce"
        return f"{points_display:4}\nGames  = {self.games[0]:4} {self.games[1]:4}\nSets   = {self.sets[0]:4} {self.sets[1]:4}"

    def update_scoreboard(self, score):
        for winner in score:
            self.update_point(winner)
    
    def update_point(self, winner):
        if winner == 'A':
            self.points[0] += 1
        else: 
            self.points[1] += 1

        game_status = self.check_game_status()
        if game_status != 'N':
            self.update_game(game_status)
            self.points = [0, 0]

    def check_game_status(self):
        def A_won_game():
            return self.points[0] >= 4 and self.points[0] - self.points[1] >= 2
        def B_won_game():
            return self.points[1] >= 4 and self.points[1] - self.points[0] >= 2
        
        if A_won_game():    return 'A'
        elif B_won_game():  return 'B'
        else:               return 'N'

    def update_game(self, winner):
        if winner == 'A':
            self.games[0] += 1
        else:
            self.games[1] += 1
        
        set_status = self.check_set_status()
        if set_status != 'N':
            self.update_set(set_status)
            self.games = [0, 0]

    def check_set_status(self):
        def A_won_set():
            return self.games[0] >= 6 and self.games[0] - self.games[1] >= 2
        def B_won_set():
            return self.games[1] >= 6 and self.games[1] - self.games[0] >= 2

        if A_won_set():     return 'A'
        elif B_won_set():   return 'B'
        else:               return 'N'

    def update_set(self, winner):
        if winner == 'A':
            self.sets[0] += 1
        else:
            self.sets[1] += 1

p = score_board()
p.update_scoreboard("ABABABA")
print(p)

