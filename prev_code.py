''' Tennis Score Board '''

Score_board = { 'points': [0,0],
                'games': [0,0],
                'sets': [0,0],
              }

def reset_score():
    for key in Score_board:
        Score_board[key] = [0,0]

def diff(score: list) -> int:
    return score[0] - score[1]

def update_points(winner: str):
    if winner == 'A':
        Score_board['points'][0] += 1
    else:
        Score_board['points'][1] += 1

def A_won_game():
    return Score_board['points'][0] >= 4 and diff(Score_board["points"]) >= 2

def B_won_game():
    return Score_board['points'][1] >= 4 and diff(Score_board['points']) <= -2

def is_game_over():
    if A_won_game():    return 'A'
    elif B_won_game():  return 'B'
    else:               return 'N'

def reset_points():
	Score_board['points'] = [0,0]

def update_games(winner: str):
    if winner == "A":
        Score_board['games'][0] += 1
    else:
        Score_board['games'][1] += 1
    reset_points()

def A_won_set():
    return Score_board['games'][0] >= 6 and diff(Score_board["points"]) >= 2

def B_won_set():
    return Score_board['games'][0] >= 6 and diff(Score_board["points"]) <= -2

def is_set_over():
    if A_won_set():	    return 'A'
    elif B_won_set():   return 'B'
    else:	            return 'N'

def reset_games():
    Score_board['games'] = [0,0]

def update_sets(winner: str):
    if winner == 'A':
        Score_board['sets'][0] += 1
    else:
        Score_board['sets'][1] += 1
    reset_games()

def update_score_board(score: str):
    reset_score()
    for point in score:
        update_points(point)
        game_status = is_game_over()
        if game_status != 'N':
            update_games(game_status)
            set_status = is_set_over()
            if set_status != "N":
                update_sets(set_status)


score = "ABABAAABBBABABABBAABAAABABABABABAABABABBABAABBBBAAAABABABABBAAABABABAABBBABABABABABA"
update_score_board(score)
print(Score_board)
reset_score()
print(Score_board)

